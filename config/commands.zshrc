# docker shell
dcshell() {
  docker compose exec $1 sh
}
dcbash() {
  docker compose exec $1 bash
}

ytdl() {
  youtube-dl \
    --download-archive archive.txt \
    --extract-audio \
    --audio-format mp3 \
    --embed-thumbnail \
    --add-metadata -o "%(artist)s - %(title)s.%(ext)s"
}

update() {
  echo "Updating apt packages..."
  sudo apt-get update &&
    sudo apt-get upgrade &&
    sudo apt-get autoremove &&
    sudo apt-get clean
  echo

  if command -v snap &>/dev/null; then
    echo "Snap command found. Updating all snaps..."
    sudo snap refresh
    echo
  fi

  echo "Pulling zsh repo..."
  git -C $ZSH_FOLDER pull
}
