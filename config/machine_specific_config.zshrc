# allow a configuration per machine which is not included in git
if [ -f "$ZSH_MACHINE_SPECIFIC" ]; then
  source "$ZSH_MACHINE_SPECIFIC"
else
  echo "No machine specific config found in ${ZSH_MACHINE_SPECIFIC}."
  echo "# machine specific ZSH Configuration" > "$ZSH_MACHINE_SPECIFIC"
  echo "Created machine specific config file at $ZSH_MACHINE_SPECIFIC."
fi