# basic aliases
alias cdh='cd ~'
alias cl='clear'
alias nano='nano -l'
alias up='uptime'

# python aliases
alias pip='pip3'
alias py='python'
alias python='python3'

# git aliases
alias gs='git status'

# docker aliases
alias docker-compose='docker compose' #! legacy
alias dc='docker compose'
alias ds='docker stats'
alias dstats='docker stats'
alias dcs='docker compose stats'
alias dcstats='docker compose stats'
alias dcbuild='docker compose build'
